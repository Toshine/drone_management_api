const Router = require('express').Router();
const Controller = require('../../controllers')

Router.post('/drones', Controller.storeNewDrone)

module.exports = Router