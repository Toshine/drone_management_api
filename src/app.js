const express = require('express')
const cors = require('cors')
const helmet = require('helmet')
const config = require('./config')
const { logger } = require('./lib/logger')
const dbConnection   = require('./database/connection')
const route = require('./routes/v1')

const app = express();

app.use(cors());
app.use(helmet());
app.use(express.json());

const PORT = config.app.port
app.use('/api/v1', route)

app.listen(PORT, () => {
  logger.info(`server has started and is running on port: ${PORT}`),
  dbConnection()
})

module.exports = app;