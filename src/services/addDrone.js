const validatorWrapper = require('../validation/validator')
const models = require('../database/models');
const { modelEnums, num, stateEnums } = require('../validation/type');
const { generateString } = require('../lib/utils')


/**
   * @Description This adds new drone data to the database
   * @Params req.body.model {Enum} as in droneModels
   * @Params req.body.battery_capacity {Float}
   * @Params req.body.serial_number {String}
   * @Params req.body.weight_limit {Float}
**/

module.exports = validatorWrapper({
  params: {
    model: { ...modelEnums },
    weight_limit: { ...num },
    battery_capacity: { ...num },
    state: { ...stateEnums },
    $$strict: true,
  },
  async handler(params) {
    const code = await generateString(12);
    const serialNumber = `MD${code.toUpperCase()}`;
    const data = Object.assign(params, { serial_number: serialNumber })
    
    return await models.drone.create(data)
  }
})