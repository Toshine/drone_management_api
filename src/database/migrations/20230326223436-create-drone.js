'use strict';
const { droneModel, droneState } = require('../enum')
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('drones', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      serial_number: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      model: {
        type: Sequelize.ENUM,
        values: Object.values(droneModel),
        allowNull: true,
      },
      weight_limit: {
        type: Sequelize.FLOAT,
        allowNull: true,
      },
      battery_capacity: {
        type: Sequelize.INTEGER
      },
      state: {
        type: Sequelize.ENUM,
        values: Object.values(droneState),
        defaultValue: droneState.IDLE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable('drones');
  }
};