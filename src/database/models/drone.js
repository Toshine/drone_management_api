'use strict';
const {
  Model
} = require('sequelize');
const { droneModel, droneState } = require('../enum');
module.exports = (sequelize, DataTypes) => {
  class drone extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // static associate(models) {
    // define association here
    // }
  }
  drone.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    serial_number: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
    model: {
      type: DataTypes.ENUM,
      values: Object.values(droneModel),
    },
    weight_limit: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    battery_capacity: {
      type: DataTypes.FLOAT,
      defaultValue: 100.0,
    },
    state: {
      type: DataTypes.ENUM,
      values: Object.values(droneState),
      defaultValue: droneState.IDLE,
    },
  }, {
    sequelize,
    timestamps: true,
    modelName: 'drone',
  });
  return drone;
};