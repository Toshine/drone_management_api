const { Sequelize } = require('sequelize');
const { logger } = require('../lib/logger')
const config  = require('./config')


const sequelize = new Sequelize(config.development)
 
const dbConnection = async () => {
  try {
    await sequelize.authenticate();
    logger.info('Connection has been established successfully.');
  } catch (error) {
    logger.error('Unable to connect to the database:', error);
  }
}

module.exports = dbConnection;