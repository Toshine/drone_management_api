module.exports = {
  droneModel: {
    Lightweight: "Lightweight",
    Middleweight: "Middleweight",
    Cruiserweight: "Cruiserweight",
    Heavyweight: "Heavyweight",
  },

  droneState: {
    IDLE: "IDLE",
    LOADING: "LOADING",
    LOADED: "LOADED",
    DELIVERING: "DELIVERING",
    DELIVERED: "DELIVERED",
    RETURNING: "RETURNING",
  },
};
