const { logger } = require('../lib/logger')
const DroneService = require('../services');


module.exports.storeNewDrone = async (req, res) => {
  try {
    const drone = await DroneService.addDrone({
      ...req.body
    })
    
    res.send({
      status: 'success',
      message: 'Drone added successfully',
      data: drone
    })
  } catch (error) {
    console.log(error)
    throw error 
  }
}