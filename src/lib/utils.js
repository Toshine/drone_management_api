exports.generateString = (keyLength) => {
  let i;
  let key = '';
  const characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  
  const charactersLength = characters.length;
  
  for (i = 0; i < keyLength; i++) {
    key += characters.substr(
      Math.floor(Math.random() * charactersLength + 1),
      1,
    );
  }
  
  return key;
};