class AppError extends Error {
  constructor(message, status = 400) {
    super(message);
    this.message = message;
    this.status = status;
    this.name = AppError.constructor.name;
  }
}
  
class ValidationError extends AppError {
  constructor(message, status = 422) {
    super(message, status);
    this.message = message.map((err) => err.message).join(', ');
  }
}
  
class NotFoundError extends AppError {
  constructor(message, status = 404) {
    super(message, status);
  }
}
  
const notFoundHandler = (req, res) => {
  return res.status(404).send({
    status: 'error',
    message: 'endpoint not found',
  });
};
  
const errorHandler = (err, req, res, next) => {
  console.log(err);
  if (res.headersSent) {
    return next(err);
  }
  switch (err.name) {
  case 'AppError':
    return res.status(err.status).send({
      status: 'error',
      message: err.message,
    });
  case 'NotFoundError':
    return res.status(err.status).send({
      status: 'error',
      message: err.message,
    });
  case 'ValidationError':
    return res.status(err.status).send({
      status: 'error',
      message: err.message,
      errors: err.errors,
    });
  default:
    return res.status(err.status || 500).send({
      status: 'error',
      message: 'an error occurred',
      ...(process.env.NODE_ENV === 'dev'
        ? {}
        : { error: err.message || err.toString() }),
    });
  }
};
  
module.exports = {
  ValidationError,
  NotFoundError,
  AppError,
  notFoundHandler,
  errorHandler,
};
  