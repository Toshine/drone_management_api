require('dotenv').config();

const config = {
  app: {
    port: process.env.NODE_PORT
  }
}

module.exports = config