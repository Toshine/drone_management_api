
module.exports = {
  string: { type: 'string', empty: false, trim: true },
  modelEnums: { type: 'string', enum: ['Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight'] },
  stateEnums: { type: 'string', enum: ['IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING'] },
  num: { type: 'number' }
}