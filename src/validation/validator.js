const Validator = require('fastest-validator');
const validator = new Validator();

const validatorWrapper = (action) => {
  const validate = action.params
    ? validator.compile(action.params)
    : null;

  return async function (params = {}) {
    if (validate) {
      const errors = validate(params);
      if (Array.isArray(errors)) {
        throw new Error(errors);
      }
    }
    return action.handler(params);
  };
};

module.exports = validatorWrapper;